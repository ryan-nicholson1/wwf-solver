# -*- coding: utf-8 -*-
"""
Created on Wed Oct  7 15:39:53 2020

@author: rdn04
"""
import itertools
import os

os.chdir('C:/Users/rdn04/Programming/wwf-solver') 

def readFile(fname):
    resultList = []
    f = open(fname, 'r')
    line_num = 1
    for line in f:
        if line_num in [1, 2]:
            line_num += 1
            continue
        line = line.rstrip('\n')
        sVals = line.split('\t')
        resultList.append(sVals)
        line_num += 1
    f.close()
    return resultList

def extractFirst(lst):
    return [item[0].lower() for item in lst]

def getLetters(word):
    return [char for char in word]

def binarySearch(lst, val):
    first = 0
    last = len(lst) - 1
    index = -1
    while (first <= last) and (index == -1):
        mid = (first + last) // 2
        if lst[mid] == val:
            index = mid
        elif val < lst[mid]:
            last = mid -1
        else:
            first = mid +1
    return index

def fibonacciSearch(lst, val):
    fibM_minus_2 = 0
    fibM_minus_1 = 1
    fibM = fibM_minus_1 + fibM_minus_2
    while (fibM < len(lst)):
        fibM_minus_2 = fibM_minus_1
        fibM_minus_1 = fibM
        fibM = fibM_minus_1 + fibM_minus_2
    index = -1;
    while (fibM > 1):
        i = min(index + fibM_minus_2, (len(lst)-1))
        if (lst[i] < val):
            fibM = fibM_minus_1
            fibM_minus_1 = fibM_minus_2
            fibM_minus_2 = fibM - fibM_minus_1
            index = i
        elif (lst[i] > val):
            fibM = fibM_minus_2
            fibM_minus_1 = fibM_minus_1 - fibM_minus_2
            fibM_minus_2 = fibM - fibM_minus_1
        else:
            return i
    if(fibM_minus_1 and index < (len(lst)-1) and lst[index+1] == val):
        return index+1;
    return -1

dic = readFile("import/Collins Scrabble Words (2019) with definitions.txt")

dic_words = extractFirst(dic)

wordList = []

point_vals = [['a', 1],
              ['b', 4],
              ['c', 4],
              ['d', 2],
              ['e', 1],
              ['f', 4],
              ['g', 3],
              ['h', 3],
              ['i', 1],
              ['j', 10],
              ['k', 5],
              ['l', 2],
              ['m', 4],
              ['n', 2],
              ['o', 1],
              ['p', 4],
              ['q', 10],
              ['r', 1],
              ['s', 1],
              ['t', 1],
              ['u', 2],
              ['v', 5],
              ['w', 4],
              ['x', 8],
              ['y', 3],
              ['z', 10]]

letter_list = input('Enter your letters: ')
must_include = input('Does the word need to include certain strings (enter no if not): ')

for i in range(len(letter_list)):  
    tups = set(itertools.permutations(letter_list,i+1)) #list
    for i in tups:
        word = ''.join(i)
        if binarySearch(dic_words, word) != -1:
            wordList.append(word)
        else:
            continue
        
for i in range(len(wordList)):
    if must_include != 'no' and must_include not in wordList[i][0]:
        continue
    else:
        charList = getLetters(wordList[i])
        pt_val = 0
        for char in charList:
            idx = binarySearch(extractFirst(point_vals), char)
            pt_val += point_vals[idx][1]
        if i == 0:
            print('\n')
            print('2-letter words')
            print(wordList[i] + ' (' + str(pt_val) + ')')    
        elif len(wordList[i]) != len(wordList[i-1]):
            print('\n')
            print(str(len(wordList[i])) + '-letter words')
            print(wordList[i] + ' (' + str(pt_val) + ')')     
        else:
            print(wordList[i] + ' (' + str(pt_val) + ')')    
     
        


